package com.bx.implatform.exception;

import com.bx.implatform.enums.ResultCode;
import lombok.Data;

import java.io.Serializable;

@Data
public class GlobalException extends RuntimeException implements Serializable {
    private static final long serialVersionUID = 8134030011662574394L;
    private Integer code;
    private String message;
/* @NotNull(message = "群聊id不可为空")
    @ApiModelProperty(value = "群聊id")
    private Long groupId;

    @Length(max = 1024, message = "发送内容长度不得大于1024")
    @NotEmpty(message = "发送内容不可为空")
    @ApiModelProperty(value = "发送内容")
    private String content;

    @NotNull(message = "消息类型不可为空")
    @ApiModelProperty(value = "消息类型")
    private Integer type;

    @ApiModelProperty(value = "是否回执消息")
    private Boolean receipt = false;

    @Size(max = 20, message = "一次最多只能@20个小伙伴哦")
    @ApiModelProperty(value = "被@用户列表")
    private List<Long> atUserIds;*/
    public GlobalException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public GlobalException(ResultCode resultCode, String message) {
        this.code = resultCode.getCode();
        this.message = message;
    }

    public GlobalException(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMsg();
    }

    public GlobalException(String message) {
        this.code = ResultCode.PROGRAM_ERROR.getCode();
        this.message = message;
    }

}
