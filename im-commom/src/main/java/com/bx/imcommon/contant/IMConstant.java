package com.bx.imcommon.contant;

public final class IMConstant {

    private IMConstant() {
    }

    /**
     * 在线状态过期时间 600s
     */
    public static final long ONLINE_TIMEOUT_SECOND = 600;
    /**
     * 消息允许撤回时间 300s
     */
    public static final long ALLOW_RECALL_SECOND = 300;
/* public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        // 设置值（value）的序列化采用FastJsonRedisSerializer
        redisTemplate.setValueSerializer(fastJsonRedisSerializer());
        redisTemplate.setHashValueSerializer(fastJsonRedisSerializer());
        // 设置键（key）的序列化采用StringRedisSerializer。
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.afterPropertiesSet();
        return redisTemplate;*/

}
